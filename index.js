// 3
let numberA = (2);
const getCube = Math.pow(numberA,3);

// 4
console.log(`The cube of ${numberA} is ${getCube}.`);

// 5 & 6
const fullAddress = ["258 Washington Ave NW", "California", "90011"];
const homeAddress = [streetAdd, stateAdd, zipAdd] = fullAddress;
console.log(`I live at ${streetAdd}, ${stateAdd} ${zipAdd}`);

// 7 & 8
const animal = {
	animalName: "Lolong",
	animalType: "Saltwater Crocodile",
	animalWeight: "1075 kgs",
	animalMeasure: "20ft 3in"
}
console.log(`${animal.animalName} was a ${animal.animalType}. He weighed at ${animal.animalWeight} with a measurement of ${animal.animalMeasure}.`);

// 9 & 10 & 11
const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((x, y) => x + y);
console.log(reduceNumber);

// 12
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "Hashi";
myDog.age = 1;
myDog.breed = "Maltese";
console.log(myDog);

const myNewDog = new Dog("Saphire", 1, "Maltese");
console.log(myNewDog);








